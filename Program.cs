﻿using System;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            Console.WriteLine("Does 2 + 2 = 4");
            Console.WriteLine("Please enter true or false");

            if (bool.Parse(Console.ReadLine()))
            {
                Console.WriteLine("WOW, you so smrt");
            }
            else{
                Console.WriteLine("Please go to school and prove to Daddy you are not a fool.");
            }

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
